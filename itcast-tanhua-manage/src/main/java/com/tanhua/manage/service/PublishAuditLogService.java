package com.tanhua.manage.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tanhua.manage.mapper.PublishAuditLogMapper;
import com.tanhua.manage.pojo.PublishAuditLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PublishAuditLogService extends ServiceImpl<PublishAuditLogMapper, PublishAuditLog> {
}
