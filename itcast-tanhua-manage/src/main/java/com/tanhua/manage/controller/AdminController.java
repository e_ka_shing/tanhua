package com.tanhua.manage.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.tanhua.manage.exception.BusinessException;
import com.tanhua.manage.pojo.Admin;
import com.tanhua.manage.service.AdminService;
import com.tanhua.manage.util.NoAuthorization;
import com.tanhua.manage.util.UserThreadLocal;
import com.tanhua.manage.vo.AdminVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("system/users")
@Slf4j
public class AdminController {

    @Autowired
    private AdminService adminService;

    /**
     * 生成验证码图片
     *
     * @param uuid
     */
    @GetMapping("verification")
    @NoAuthorization
    public void verification(@RequestParam("uuid") String uuid, HttpServletResponse response) throws IOException {
        response.setDateHeader("Expires", 0);
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");

        // 生成验证码图片
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(299, 97);

        String code = lineCaptcha.getCode();

        log.info("生成验证码，uuid = " + uuid +", code = " + code);

        // 将验证码的值存储到redis中
        this.adminService.saveVerificationCode(uuid, code);

        // 输出验证码图片
        lineCaptcha.write(response.getOutputStream());
    }

    /**
     * 管理员登陆
     */
    @PostMapping("login")
    @NoAuthorization
    public AdminVo login(@RequestBody AdminVo adminVo) {
        String token = adminService.login(BeanUtil.toBean(adminVo, Admin.class), adminVo.getUuid(), adminVo.getVerificationCode());
        if (StrUtil.isEmpty(token)) {
            throw new BusinessException("登录出错");
        }
        AdminVo vo = new AdminVo();
        vo.setToken(token);
        return vo;
    }

    /**
     * 获取个人信息
     */
    @PostMapping("profile")
    public AdminVo profile() {
        Admin admin = UserThreadLocal.get();
        Admin result = adminService.getById(admin.getId());
        if (result == null) {
            throw new BusinessException("无效的凭据");
        }
        result.setPassword(null);//不应该返回

        return BeanUtil.toBean(result, AdminVo.class);
    }

    /**
     * 登出
     */
    @PostMapping("logout")
    public Boolean logout() {
        Admin admin = UserThreadLocal.get();
        adminService.removeToken(admin.getToken());
        UserThreadLocal.remove();
        return true;
    }

}
