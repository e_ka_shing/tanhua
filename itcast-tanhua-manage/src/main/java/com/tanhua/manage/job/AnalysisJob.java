package com.tanhua.manage.job;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.tanhua.manage.enums.LogTypeEnum;
import com.tanhua.manage.pojo.AnalysisByDay;
import com.tanhua.manage.pojo.Log;
import com.tanhua.manage.pojo.User;
import com.tanhua.manage.service.AnalysisService;
import com.tanhua.manage.service.LogService;
import com.tanhua.manage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnalysisJob {

    @Autowired
    private AnalysisService analysisService;

    @Autowired
    private UserService userService;

    @Autowired
    private LogService logService;

    @Scheduled(cron = "0 0/1 * * * *") //每分执行，用于测试
//    @Scheduled(cron = "0 0/30 * * * *") //每30分钟执行
    public void run() {
//        String today = DateUtil.formatDate(new Date());
        String today = "2020-09-08";

        //查询当天的统计数据
        AnalysisByDay analysisByDay = this.analysisService.getOne(Wrappers.<AnalysisByDay>lambdaQuery()
                .eq(AnalysisByDay::getRecordDate, today));

        if (null == analysisByDay) {
            //当天还没有统计数据
            analysisByDay = new AnalysisByDay();
            analysisByDay.setRecordDate(DateUtil.parseDate(today));

            this.analysisService.save(analysisByDay);
        }

        //扫描log表，计算，写入到结果表

        //当天的活跃用户数
        int activeCount = this.logService.count(Wrappers.query(Log.builder().logTime(today).build())
                .select("DISTINCT(user_id)")
        );

        if (activeCount > 0) {
            analysisByDay.setNumActive(activeCount);
        }

        List<Log> logList = this.logService.list(Wrappers.query(Log.builder()
                .logTime(today)
                .type(LogTypeEnum.LOGIN.getValue())
                .build())
                .select("DISTINCT(user_id)"));

        if(CollUtil.isNotEmpty(logList)){
            //今日登录的用户数
            analysisByDay.setNumLogin(logList.size());

            //重置，重新计算
            analysisByDay.setNumRegistered(0);
            analysisByDay.setNumRetention1d(0);

            List<User> userList = this.userService.list(Wrappers.<User>lambdaQuery().in(User::getId, CollUtil.getFieldValues(logList, "userId")));
            if(CollUtil.isNotEmpty(userList)){
                for (User user : userList) {
                    long day = DateUtil.betweenDay(user.getCreated(), DateUtil.parseDate(today), true);
                    if(day == 0){
                        // 今日注册的用户
                        analysisByDay.setNumRegistered(analysisByDay.getNumRegistered() + 1);
                    }else if(day == 1){
                        //次日留存用户
                        analysisByDay.setNumRetention1d(analysisByDay.getNumRetention1d() + 1);
                    }
                }
            }
        }

        //更新数据
        this.analysisService.updateById(analysisByDay);

    }

}
